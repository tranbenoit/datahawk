﻿
namespace DataHawk.WebScraping.Models.Enum
{
    public enum ReviewState
    {
        Loading = 0,
        ErrorState = 1,
        Indexed = 2
    }
}
