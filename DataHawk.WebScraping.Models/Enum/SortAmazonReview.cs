﻿
using Newtonsoft.Json.Converters;
using System.Text.Json.Serialization;

namespace DataHawk.WebScraping.Models.Enum
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SortAmazonReview
    {
        Asin = 0,
        ReviewDate = 1,
        ReviewTitle = 2,
        ReviewContent = 3,
        Rating = 4
    }
}
