﻿using DataHawk.WebScraping.Models.Enum;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataHawk.WebScraping.Models.Interfaces
{
    public interface IAmazonService
    {
        Task<CustomerReviews> ParseReviewsFromAsins(List<string> asins, long take = 10);
        Task<IList<AmazonReview>> SortAndParseReviewsFromAsins(List<string> asins, SortAmazonReview sortType, long take = 10);
    }
}
