﻿using System.Net.Http;
using System.Threading.Tasks;

namespace DataHawk.WebScraping.Models.Interfaces
{
    public interface IRestService
    {
        Task<HttpResponseMessage> GetAsync(string requestUrl);
    }
}
