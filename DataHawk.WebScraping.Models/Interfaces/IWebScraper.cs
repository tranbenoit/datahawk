﻿using DataHawk.WebScraping.Models.Dto;
using System.Collections.Generic;

namespace DataHawk.WebScraping.Models.Interfaces
{
    public interface IWebScraper
    {
        IList<AmazonReviewDto> ParseHtml(string html);
        int GetNumberOfReviews(string html);
    }
}
