﻿using DataHawk.WebScraping.Services.Helper;
namespace DataHawk.WebScraping.Models.Dto
{
    public class AmazonReviewDto
    {
        public string Author { get; set; } //unrequired but thought it would matter to get this data
        public string Title { get; set; }
        public string Rating { get; set; }
        public string DateReview { get; set; }
        public string Content { get; set; }

        public AmazonReview ToAmazonReview(string asin)
        {
            var amazonReview = new AmazonReview
            {
                Asin = asin,
                Author = Author,
                Title = Title,
                Content = Content,
                PublicationDate = AmazonReviewHelper.GetReviewDateFromRawData(DateReview),
                Rating = AmazonReviewHelper.GetReviewRatingFromRawData(Rating)
            };
            AmazonReviewHelper.SetReviewState(amazonReview);
            return amazonReview;
        }
    }
}
