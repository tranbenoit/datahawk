﻿using DataHawk.WebScraping.Models;
using DataHawk.WebScraping.Models.Dto;
using DataHawk.WebScraping.Models.Enum;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace DataHawk.WebScraping.Services.Helper
{
    public static class AmazonReviewHelper
    {
        private const string RegexPatternAmazonReviewDate = "(reviewed in ).+( on )(?<date>(september|october|november|december|january|february|march|april|may|june|july|august) [0-9]{1,2}, [0-9]{4})";
        private const string RegexPatternAmazonRating = @"(?<rating>[0-9\.]{1,3}) out of [0-9\.]{1,3} star[s]?";
        private const string RegexPatternReviewsNumber = @"[^|]+\|((?<numberReviews>[0-9]+)\w+)";

        public static DateTime GetReviewDateFromRawData(string value)
        {
            try
            {
                var regex = new Regex(RegexPatternAmazonReviewDate, RegexOptions.IgnoreCase);
                var groups = regex.Match(value).Groups;
                return DateTime.Parse(groups["date"]?.Value);
            }
            catch(Exception)
            {
                return DateTime.MinValue;
            }
        }

        public static long GetReviewRatingFromRawData(string value)
        {
            try
            {
                var regex = new Regex(RegexPatternAmazonRating);
                var groups = regex.Match(value).Groups;
                return Convert.ToInt32(double.Parse(groups["rating"]?.Value, CultureInfo.InvariantCulture));
            }
            catch(Exception)
            {
                return -1;
            }
        }

        public static void SetReviewState(AmazonReview amazonReview)
        {
            if (string.IsNullOrEmpty(amazonReview.Asin) 
                || string.IsNullOrEmpty(amazonReview.Author)
                || string.IsNullOrEmpty(amazonReview.Content)
                || string.IsNullOrEmpty(amazonReview.Title)
                || amazonReview.Rating == -1
                || amazonReview.PublicationDate == DateTime.MinValue)
                amazonReview.State = ReviewState.ErrorState;
            amazonReview.State = ReviewState.Indexed;
        }

        public static int GetReviewsNumberFromRawData(string value)
        {
            var removedWhiteSpace = Regex.Replace(value, @"\s", string.Empty);
            return int.Parse(Regex.Match(removedWhiteSpace, RegexPatternReviewsNumber).Groups["numberReviews"]?.Value);
        }
    }
}
