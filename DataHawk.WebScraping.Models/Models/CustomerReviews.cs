﻿using System.Collections.Generic;

namespace DataHawk.WebScraping.Models
{
    public class CustomerReviews
    {
        public long NumberReviewsSelected { get; set; }
        public long NumberReviewsRetrieved { get => Reviews.Count; }
        public List<string> AsinRequested { get; set; }
        public List<ResponseAmazonReview> Reviews { get; set; }

        public CustomerReviews(IEnumerable<string> asins, long take)
        {
            AsinRequested = new List<string>(asins);
            Reviews = new List<ResponseAmazonReview>();
            NumberReviewsSelected = take;
        }
    }
}
