﻿using DataHawk.WebScraping.Models.Enum;
using System;

namespace DataHawk.WebScraping.Models
{
    public class AmazonReview
    {
        public string Asin { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public long Rating { get; set; }
        public DateTime PublicationDate { get; set; }
        public string Content { get; set; }
        public ReviewState State { get; set; }
    }
}
