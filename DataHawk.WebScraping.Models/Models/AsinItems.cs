﻿using System.Collections.Generic;

namespace DataHawk.WebScraping.Models
{
    public class AsinItems
    {
        public List<string> Asins { get; set; }
    }
}
