﻿using System.Collections.Generic;

namespace DataHawk.WebScraping.Models
{
    public class ResponseAmazonReview
    {
        public string Asin { get; set; }
        public List<AmazonReview> Reviews { get; set; }
        public int NumberReviewsRetrieved { get => Reviews.Count; }
        public ResponseAmazonReview(string asin)
        {
            Asin = asin;
            Reviews = new List<AmazonReview>();
        }
    }
}
