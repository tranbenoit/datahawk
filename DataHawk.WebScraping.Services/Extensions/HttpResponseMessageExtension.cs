﻿using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataHawk.WebScraping.Services.Extensions
{
    public static class HttpResponseMessageExtension
    {
        public static async Task<string> GetHtmlFromResponse(this HttpResponseMessage response)
        {
            var htmlResult = string.Empty;
            using (var responseStream = await response.Content.ReadAsStreamAsync())
            {
                if(response.Content.Headers.ContentEncoding.Contains("gzip"))
                {
                    using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
                    using (var streamReader = new StreamReader(decompressedStream))
                        htmlResult = await streamReader.ReadToEndAsync();
                }
                else
                    using (var streamReader = new StreamReader(responseStream))
                        htmlResult = await streamReader.ReadToEndAsync();
            }

            return htmlResult;
        }
    }
}
