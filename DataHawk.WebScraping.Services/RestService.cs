﻿using DataHawk.WebScraping.Models.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataHawk.WebScraping.Services
{
    public class RestService : IRestService
    {
        public RestService()
        {
        }

        public async Task<HttpResponseMessage> GetAsync(string requestUrl)
        {
            var client = new HttpClient();
            var response = await client.GetAsync(requestUrl);
            client.Dispose();
            
            return response;
        }
    }
}
