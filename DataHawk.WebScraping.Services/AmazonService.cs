﻿using DataHawk.WebScraping.Models;
using DataHawk.WebScraping.Models.Enum;
using DataHawk.WebScraping.Models.Interfaces;
using DataHawk.WebScraping.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DataHawk.WebScraping.Services
{
    public class AmazonService : IAmazonService
    {
        private const string productReviewUrl = "https://www.amazon.com/product-reviews/";
        private const string productReviewParameterMostRecent = "/ref=cm_cr_arp_d_viewopt_srt?sortBy=recent&pageNumber=";
        private readonly IRestService _restService;
        private readonly IWebScraper _webScraper;

        public AmazonService(IRestService restService, IWebScraper webScraper)
        {
            _restService = restService ?? throw new ArgumentNullException(nameof(restService));
            _webScraper = webScraper ?? throw new ArgumentNullException(nameof(WebScraper));
        }

        public IWebScraper WebScraper { get; }

        public async Task<CustomerReviews> ParseReviewsFromAsins(List<string> asins, long take = 10)
        {
            var result = new CustomerReviews(asins, take);
            for (var i = 0; i < asins.Count; i++)
            {
                result.Reviews.Add(await ParseReviewFromAsin(asins[i], take));
            }
            return result;
        }

        public async Task<IList<AmazonReview>> SortAndParseReviewsFromAsins(List<string> asins, SortAmazonReview sortType, long take = 10)
        {
            var parsedAmazonReviews = await ParseReviewsFromAsins(asins, take);
            var result = new List<AmazonReview>();
            for(var i = 0; i< parsedAmazonReviews.Reviews.Count; i++)
            {
                switch(sortType)
                {
                    case SortAmazonReview.Rating:
                        result.AddRange(parsedAmazonReviews.Reviews[i].Reviews.OrderBy(review => review.Rating));
                        break;
                    case SortAmazonReview.ReviewContent:
                        result.AddRange(parsedAmazonReviews.Reviews[i].Reviews.OrderBy(review => review.Content));
                        break;
                    case SortAmazonReview.ReviewDate:
                        result.AddRange(parsedAmazonReviews.Reviews[i].Reviews.OrderBy(review => review.PublicationDate));
                        break;
                    case SortAmazonReview.ReviewTitle:
                        result.AddRange(parsedAmazonReviews.Reviews[i].Reviews.OrderBy(review => review.Title));
                        break;
                    case SortAmazonReview.Asin:
                        result.AddRange(parsedAmazonReviews.Reviews[i].Reviews.OrderBy(review => review.Asin));
                        break;
                    default:
                        break;
                }

            }
            return result;
        }

        private async Task<ResponseAmazonReview> ParseReviewFromAsin(string asin, long take = 10)
        {
            var result = new ResponseAmazonReview(asin);
            var pageNumber = 1;
            var maxReview = -1;
            while (maxReview == -1 || (result.Reviews.Count < take && result.Reviews.Count < maxReview))
            {
                var requestUrl = productReviewUrl + asin + productReviewParameterMostRecent + pageNumber;
                var response = await _restService.GetAsync(requestUrl);

                if (response.IsSuccessStatusCode)
                {
                    var htmlResult = await response.GetHtmlFromResponse();
                    if(maxReview == -1)
                        maxReview = _webScraper.GetNumberOfReviews(htmlResult);
                    result.Reviews.AddRange(_webScraper.ParseHtml(htmlResult).Select(review => review.ToAmazonReview(asin)));
                }
                else
                    break;

                pageNumber++;
                Thread.Sleep(1000); // avoid being blocked by captcha
            }
            
            return result;
        }
    }
}
