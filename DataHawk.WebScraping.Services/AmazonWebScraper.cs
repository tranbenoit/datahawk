﻿using DataHawk.WebScraping.Models.Dto;
using DataHawk.WebScraping.Models.Interfaces;
using DataHawk.WebScraping.Services.Helper;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;

namespace DataHawk.WebScraping.Services
{
    public class AmazonWebScraper : IWebScraper
    {
        public IList<AmazonReviewDto> ParseHtml(string html)
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);
            
            var reviewNodes = htmlDoc.GetElementbyId("cm_cr-review_list").Descendants().Where(n => n.Attributes["data-hook"]?.Value == "review");

            var reviews = new List<AmazonReviewDto>();

            foreach(var review in reviewNodes)
            {
                var dataReview = review.Descendants().ToList();
                reviews.Add(new AmazonReviewDto
                {
                    Author = dataReview.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "genome-widget")?.InnerText.Trim(),
                    Title = dataReview.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "review-title")?.InnerText.Trim(),
                    Rating = dataReview.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "review-star-rating")?.InnerText.Trim(),
                    DateReview = dataReview.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "review-date")?.InnerText.Trim(),
                    Content = dataReview.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "review-body")?.InnerText.Trim()
                });
            }

            return reviews;
        }

        public int GetNumberOfReviews(string html)
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            var reviewInfoSection = htmlDoc.GetElementbyId("filter-info-section").Descendants().FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "cr-filter-info-review-rating-count")?.InnerText.Trim();
            return AmazonReviewHelper.GetReviewsNumberFromRawData(reviewInfoSection);

        }
    }
}
