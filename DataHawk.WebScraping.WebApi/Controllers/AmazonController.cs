﻿using DataHawk.WebScraping.Models;
using DataHawk.WebScraping.Models.Enum;
using DataHawk.WebScraping.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataHawk.WebScraping.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AmazonController : ControllerBase
    {
        private readonly IAmazonService _amazonService;

        public AmazonController(IAmazonService amazonService)
        {
            _amazonService = amazonService;
        }

        /// <summary>
        /// Enter product's asins as a json request and add a take query parameter to set how many reviews you wish to get
        /// </summary>
        /// <returns></returns>
        [HttpPost("GetRecentReviews")]
        public async Task<CustomerReviews> GetRecentReviews(AsinItems asinItems, long take = 10)
        {
            return await _amazonService.ParseReviewsFromAsins(asinItems.Asins, take);
        }

        /// <summary>
        /// Enter product's asins and select sort type to get reviews in a particular order
        /// </summary>
        /// <returns></returns>
        [HttpPost("GetRecentReviewsSorted")]
        public async Task<IList<AmazonReview>> GetRecentViewsSorted(AsinItems asinItems, SortAmazonReview sortType, int take = 10)
        {
            return await _amazonService.SortAndParseReviewsFromAsins(asinItems.Asins, sortType, take);
        }
    }
}
