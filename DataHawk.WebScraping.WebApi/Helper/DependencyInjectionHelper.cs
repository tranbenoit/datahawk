﻿using DataHawk.WebScraping.Models.Interfaces;
using DataHawk.WebScraping.Services;
using Microsoft.Extensions.DependencyInjection;

namespace DataHawk.WebScraping.WebApi.Helper
{
    public static class DependencyInjectionHelper
    {
        public static void InjectServices(IServiceCollection services)
        {
            services.AddScoped<IRestService, RestService>();
            services.AddScoped<IAmazonService, AmazonService>();
            services.AddScoped<IWebScraper, AmazonWebScraper>();
        }
    }
}
