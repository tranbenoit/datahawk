using DataHawk.WebScraping.Services.Extensions;
using DataHawk.WebScraping.Services.Helper;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework.Internal;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataHawk.WebScraping.Services.Test
{
    [TestClass]
    public class AmazonServiceTests
    {
        [TestMethod]
        public async Task ParsingHtmlTest()
        {
            // setup
            var restService = new RestService();
            var asin = "B082XY23D5";
            var productReviewUrl = "https://www.amazon.com/product-reviews/";

            // execute
            var response = await restService.GetAsync(productReviewUrl + asin);

            var htmlResult = await response.GetHtmlFromResponse();

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlResult);

            var allReviewNode = htmlDoc.GetElementbyId("cm_cr-review_list");

            var reviews = allReviewNode.Descendants().Where(n => n.Attributes["data-hook"]?.Value == "review");
            foreach (var review in reviews)
            {
                var data = review.Descendants().ToList();
                var author = data.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "genome-widget").InnerText.Trim();
                var title = data.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "review-title").InnerText.Trim();
                var rating = data.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "review-star-rating").InnerText.Trim();
                var dateReview = data.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "review-date").InnerText.Trim();
                var description = data.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "review-body").InnerText.Trim();
                var nbPeopleCommentHelpful = data.FirstOrDefault(n => n.Attributes["data-hook"]?.Value == "helpful-vote-statement").InnerText.Trim();
            }


        }

        [TestMethod]
        public void RetrieveDateFromRawDataTest()
        {
            var regexPattern = "(reviewed in ).+( on )(?<date>(september|october|november|december|january|february|march|april|may|june|july|august) [0-9]{1,2}, [0-9]{4})";
            var contentstring = "Reviewed in the United States on March 8, 2020";
            var regex = new Regex(regexPattern, RegexOptions.IgnoreCase);
            var groups = regex.Match(contentstring).Groups;
            var rawDate = groups["date"].Value;
            var date = DateTime.Parse(rawDate);
        }
    }
}
